
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local registry = {
	walk = {},
	run = {},
	sprint = {},
	turn = {},
	stop = {},
	landing = {},
	jump = {},
	gear = {},
	gearfast = {},
}

local blacklist = {
	'eft/walk_wood_11.ogg',
	'eft/walk_wood_thin_08.ogg',
	'eft/sprint_asphalt_07.ogg',
}

local function regular(target, basename, from, to, format)
	for i = from, to do
		local path = 'eft/' .. basename .. string.format(format or '%.2d', i) .. '.ogg'

		if not table.qhasValue(blacklist, path) then
			table.insert(target, path)
		end
	end
end

local function regularWalk(mattype, basename, from, to, format)
	registry.walk[mattype] = registry.walk[mattype] or {}
	regular(registry.walk[mattype], basename, from, to, format)
end

local function regularTurn(mattype, basename, from, to, format)
	registry.turn[mattype] = registry.turn[mattype] or {}
	regular(registry.turn[mattype], basename, from, to, format)
end

local function regularStop(mattype, basename, from, to, format)
	registry.stop[mattype] = registry.stop[mattype] or {}
	regular(registry.stop[mattype], basename, from, to, format)
end

local function regularRun(mattype, basename, from, to, format)
	registry.run[mattype] = registry.run[mattype] or {}
	regular(registry.run[mattype], basename, from, to, format)
end

local function regularLanding(mattype, basename, from, to, format)
	registry.landing[mattype] = registry.landing[mattype] or {}
	regular(registry.landing[mattype], basename, from, to, format)
end

local function regularSprint(mattype, basename, from, to, format)
	registry.sprint[mattype] = registry.sprint[mattype] or {}
	regular(registry.sprint[mattype], basename, from, to, format)
end

local function regularJumping(mattype, basename, from, to, format)
	registry.jump[mattype] = registry.jump[mattype] or {}
	regular(registry.jump[mattype], basename, from, to, format)
end

local function regularGear(mattype, basename, from, to, format)
	registry.gear[mattype] = registry.gear[mattype] or {}
	regular(registry.gear[mattype], basename, from, to, format)
end

local function regularGearFast(mattype, basename, from, to, format)
	registry.gearfast[mattype] = registry.gearfast[mattype] or {}
	regular(registry.gearfast[mattype], basename, from, to, format)
end

regularWalk(MAT_WOOD, 'walk_wood_', 1, 24)
regularTurn(MAT_WOOD, 'turn_wood_', 1, 5)
regularStop(MAT_WOOD, 'stop_wood_', 1, 6)
regularSprint(MAT_WOOD, 'sprint_wood_', 1, 13)
regularRun(MAT_WOOD, 'run_wood_', 1, 10)
regularLanding(MAT_WOOD, 'landing_wood_', 1, 9)
regularJumping(MAT_WOOD, 'jump_wood_', 1, 6)

-- regularWalk(MAT_TILE, 'walk_tile_', 1, 12)
-- regularTurn(MAT_TILE, 'turn_tile_', 1, 4)
-- regularStop(MAT_TILE, 'stop_tile_', 1, 4)
-- regularSprint(MAT_TILE, 'sprint_tile_', 1, 12)
-- regularRun(MAT_TILE, 'run_tile_', 1, 12)
-- regularLanding(MAT_TILE, 'landing_tile_', 1, 4)
-- regularJumping(MAT_TILE, 'jump_tile_', 1, 3)

regularWalk(MAT_METAL, 'walk_metal', 1, 6, '%d')
regularTurn(MAT_METAL, 'turn_metal', 1, 3, '%d')
regularStop(MAT_METAL, 'stop_metal', 1, 3, '%d')
regularSprint(MAT_METAL, 'sprint_metal', 1, 6, '%d')
regularRun(MAT_METAL, 'run_metal', 1, 6, '%d')
regularLanding(MAT_METAL, 'landing_metal', 1, 3, '%d')
regularJumping(MAT_METAL, 'jump_metal', 1, 3, '%d')

regularWalk(MAT_SLOSH, 'walk_puddle_', 1, 9)
regularTurn(MAT_SLOSH, 'turn_puddle_', 1, 4)
regularStop(MAT_SLOSH, 'stop_puddle_', 1, 4)
regularSprint(MAT_SLOSH, 'sprint_puddle_', 1, 10)
regularRun(MAT_SLOSH, 'run_puddle_', 1, 9)
regularLanding(MAT_SLOSH, 'landing_puddle_', 1, 4)
regularJumping(MAT_SLOSH, 'jump_puddle_', 1, 4)

regularWalk(MAT_GLASS, 'walk_whole_glass_', 1, 12)
regularTurn(MAT_GLASS, 'turn_whole_glass_', 1, 7)
regularStop(MAT_GLASS, 'stop_whole_glass_', 1, 6)
regularSprint(MAT_GLASS, 'sprint_whole_glass_', 1, 12)
regularRun(MAT_GLASS, 'run_whole_glass_', 1, 12)
regularLanding(MAT_GLASS, 'landing_whole_glass_', 1, 8)
regularJumping(MAT_GLASS, 'jump_whole_glass_', 1, 6)

local concrete = {MAT_CONCRETE, MAT_TILE}
for _, mat in ipairs(concrete) do
	regularWalk(mat, 'walk_concrete', 1, 6, '%d')
	regularTurn(mat, 'turn_concrete', 1, 3, '%d')
	regularStop(mat, 'stop_asphalt_', 1, 8)
	regularSprint(mat, 'sprint_asphalt_', 1, 17)
	regularRun(mat, 'run_concrete', 1, 6, '%d')
	regularLanding(mat, 'landing_concrete', 1, 3, '%d')
	regularJumping(mat, 'jump_concrete', 1, 3, '%d')
end

local soil = {MAT_DIRT, MAT_SAND}
for _, mat in ipairs(soil) do
	regularWalk(mat, 'soil_walk_', 1, 8)
	regularTurn(mat, 'soil_turn_', 1, 5)
	regularStop(mat, 'soil_stop_', 1, 4)
	regularSprint(mat, 'soil_sprint_', 1, 8)
	regularRun(mat, 'soil_run_', 1, 8)
	regularLanding(mat, 'soil_landing_', 1, 4)
	regularJumping(mat, 'soil_jump_', 1, 4)
end

regularWalk(MAT_GRATE, 'walk_proflist_', 1, 10)
regularTurn(MAT_GRATE, 'turn_proflist_', 1, 5)
regularStop(MAT_GRATE, 'stop_proflist_', 1, 2)
regularSprint(MAT_GRATE, 'sprint_proflist_', 1, 12)
regularRun(MAT_GRATE, 'run_proflist_', 1, 10)
regularLanding(MAT_GRATE, 'landing_proflist_', 1, 5)
regularJumping(MAT_GRATE, 'jump_proflist_', 1, 5)

regularWalk(MAT_VENT, 'walk_metal_thin_', 1, 20)
regularTurn(MAT_VENT, 'turn_metal_thin_', 1, 10)
regularStop(MAT_VENT, 'stop_metal_thin_', 1, 7)
regularSprint(MAT_VENT, 'sprint_metal_thin_', 1, 22)
regularRun(MAT_VENT, 'run_metal_thin_', 1, 19)
regularLanding(MAT_VENT, 'landing_metal_thin_', 1, 12)
regularJumping(MAT_VENT, 'jump_metal_thin_', 1, 10)

regularWalk(MAT_GRASS, 'walk2_grasslow_', 1, 8)
regularTurn(MAT_GRASS, 'turn2_grasslow_', 1, 6)
regularSprint(MAT_GRASS, 'sprint2_grasslow_', 1, 8)
regularRun(MAT_GRASS, 'run2_grasslow_', 1, 8)
regularStop(MAT_GRASS, 'stop_grasslow_', 1, 5)
regularLanding(MAT_GRASS, 'landing_grasslow_', 1, 8)
regularJumping(MAT_GRASS, 'jump_grasslow_', 1, 6)

regularWalk(MAT_PLASTIC, 'walk_plastic_', 1, 16)
regularTurn(MAT_PLASTIC, 'turn_plastic_', 1, 5)
regularSprint(MAT_PLASTIC, 'sprint_plastic_', 1, 12)
regularRun(MAT_PLASTIC, 'run_plastic_', 1, 16)
regularStop(MAT_PLASTIC, 'stop_plastic_', 1, 6)
regularLanding(MAT_PLASTIC, 'landing_plastic_', 1, 6)
regularJumping(MAT_PLASTIC, 'jump_plastic_', 1, 5)

regularWalk('gravel', 'walk_gravel_', 1, 13)
regularTurn('gravel', 'turn_gravel_', 1, 8)
regularStop('gravel', 'stop_gravel_', 1, 6)
regularSprint('gravel', 'sprint_gravel_', 1, 12)
regularRun('gravel', 'run_gravel_', 1, 8)
regularLanding('gravel', 'landing_gravel_', 1, 9)
regularJumping('gravel', 'jump_gravel_', 1, 3)

regularWalk('wood_panel', 'walk_wood_thin_', 1, 12)
regularRun('wood_panel', 'run_wood_thin_', 1, 12)
regularSprint('wood_panel', 'sprint_wood_thin_', 1, 12)
regularTurn('wood_panel', 'turn_wood_thin_', 1, 6)
regularStop('wood_panel', 'stop_wood_thin_', 1, 5)
regularJumping('wood_panel', 'jump_wood_thin_', 1, 4)
regularLanding('wood_panel', 'landing_wood_thin_', 1, 8)

regularGear(1, 'gear_stereo', 1, 20, '%d')
regularGearFast(1, 'gear_fast_stereo', 1, 6, '%d')

return registry
