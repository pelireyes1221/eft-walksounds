
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local function PopulateToolMenu()
	spawnmenu.AddToolMenuOption('Utilities', 'User', 'gui.eft_footsteps.menu', 'gui.eft_footsteps.menu', '', '', function(self)
		self:Help('gui.eft_footsteps.note_self')

		self:CheckBox('gui.eft_footsteps.cvar.cl_eft_footsteps_enable', 'cl_eft_footsteps_enable')
		self:CheckBox('gui.eft_footsteps.cvar.cl_eft_footsteps_landing', 'cl_eft_footsteps_landing')
		self:CheckBox('gui.eft_footsteps.cvar.cl_eft_footsteps_jumping', 'cl_eft_footsteps_jumping')
		self:CheckBox('gui.eft_footsteps.cvar.cl_eft_footsteps_turning', 'cl_eft_footsteps_turning')
		self:CheckBox('gui.eft_footsteps.cvar.cl_eft_footsteps_stopping', 'cl_eft_footsteps_stopping')
		self:CheckBox('gui.eft_footsteps.cvar.cl_eft_footsteps_gear', 'cl_eft_footsteps_gear')
	end)

	spawnmenu.AddToolMenuOption('Utilities', 'Admin', 'gui.eft_footsteps.menu_sv', 'gui.eft_footsteps.menu_sv', '', '', function(self)
		self:Help('gui.eft_footsteps.note_server')

		self:CheckBox('gui.eft_footsteps.cvar.sv_eft_footsteps_enable', 'sv_eft_footsteps_enable')
		self:CheckBox('gui.eft_footsteps.cvar.sv_eft_footsteps_landing', 'sv_eft_footsteps_landing')
		self:CheckBox('gui.eft_footsteps.cvar.sv_eft_footsteps_jumping', 'sv_eft_footsteps_jumping')
		self:CheckBox('gui.eft_footsteps.cvar.sv_eft_footsteps_turning', 'sv_eft_footsteps_turning')
		self:CheckBox('gui.eft_footsteps.cvar.sv_eft_footsteps_stopping', 'sv_eft_footsteps_stopping')
		self:CheckBox('gui.eft_footsteps.cvar.sv_eft_footsteps_gear', 'sv_eft_footsteps_gear')
	end)
end

hook.Add('PopulateToolMenu', 'EFTFootsteps', PopulateToolMenu)
